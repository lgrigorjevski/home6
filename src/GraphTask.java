import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      // здесь тесты
      System.out.println(Arrays.toString(convertTreeToPruferCode(null)));
      createGraphAndTestPruferCode(5);
      createGraphAndTestPruferCode(0);
      createGraphAndTestPruferCode(1);
      createGraphAndTestPruferCode(3);
      createGraphAndTestPruferCode(7);

      // tekitame puu 1000-tipudega, loeme koodi sisse
      Graph g = new Graph("G");
      g.createRandomTree(1000);
      int[] code = convertTreeToPruferCode(g);
      System.out.println(String.format("1000 vertex tree code. length: %d", code.length));

      /// tekitame puu 2000-tipudega, loeme koodi sisse ja saame aega millisekundides
      g = new Graph("Big tree");
      g.createRandomTree(2000);
      long t1 = System.currentTimeMillis();
      code = convertTreeToPruferCode(g);
      long t2 = System.currentTimeMillis();
      System.out.println(String.format("2000 vertex tree code. length: %d. time: %d ms", code.length, t2-t1));
   }

   /**
    * creates graph with given vertex count and prints result
    * @param vertexCount
    */
   private void createGraphAndTestPruferCode(int vertexCount) {
      // meetod testi jaoks
      // tekitame puu määratud tipude arvuga
      Graph g = new Graph("G");
      g.createRandomTree(vertexCount);
      // trukime valja saadud puu ja selle pruferi kood
      System.out.println(g);
      System.out.println(Arrays.toString(convertTreeToPruferCode(g)));
   }

   /**
    * turns tree to Prufer code
    * @param tree
    * @return Prufer code to that tree
    */
   // peamine meetod et pruferi koodi leida
   public int[] convertTreeToPruferCode(Graph tree) {
      // kui 0 siis tagastame ka 0
      if(tree == null)
         return null;

      // kui kaks voi vahem tipe siis kood on tyhi
      int[][] adjacencyMatrix = tree.createAdjMatrix();
      if(adjacencyMatrix.length <= 2)
         return new int[] {};

      // loome massiivi tulemise jaoks
      // peaks olema kahe vorra vahem kui tippe
      int codeSize = adjacencyMatrix.length - 2;
      int[] result = new int[codeSize];

      // itereerime selleks et leida iga koodi arvu
      for(int i = 0; i < codeSize; ++i) {
         // leiame tipu- ja lehe väiksema arvuga ning selle no naabri
         int[] smallestLeafAndNeighbour = findSmallestLeafAndNeighbour(adjacencyMatrix);
         // вычёркиваем её из дерева
         markVertexRemoved(adjacencyMatrix, smallestLeafAndNeighbour[0]);
         // kirjutame tulemuse sisse naabri indeksi + 1, sest et
         // koodis on indeksi alates nullist kuni n-1, aga
         // reaalarvud alates 1 kuni n
         result[i] = smallestLeafAndNeighbour[1] + 1;
      }
      return result;
   }

   /**
    * gets number of the smallest leaf in tree represented by adjacency matrix
    * @param adjacencyMatrix
    * @return array of 2 numbers - number of vertex to remove and number of its neighbour
    */
   private int[] findSmallestLeafAndNeighbour(int[][] adjacencyMatrix) {
      // leiame lehe- ja tipu kõige väiksema indeksiga
      // kõigepealt paneme maksinum arvu, selleks et saaks vorrelda
      int minVertexNumber = Integer.MAX_VALUE;
      int neighbourNumber = 0;

      int neighboursCount;
      int lastNeighbourNumber;

      int length = adjacencyMatrix.length;
      // itereerime maatriksi jargi
      for(int i = 0; i < length; ++i) {
         // omistame nulliga muutujad mis on naabri arv ja naabri indeks
         neighboursCount = 0;
         lastNeighbourNumber = 0;
         for(int j = 0; j < length; ++j) {
            //kui on 1 siis on tipud uhendatud ja me kirjutame ulesse selle naabri indeksi
            //ja inkrementeerime naabrite arvu
            if(adjacencyMatrix[i][j] == 1) {
               neighboursCount++;
               lastNeighbourNumber = j;
            }
         }
         //kui oleks vaid 1 naaber siis see on leht
         //ja kui selle lehe indes on väiksem kui juba olemasolev, siis kirjutame selle ule
         if(neighboursCount == 1 && i < minVertexNumber) {
            minVertexNumber = i;
            neighbourNumber = lastNeighbourNumber;
         }
      }
      // tagastame vaiksema lehe indeksi ja selle naabri
      return new int[] {minVertexNumber, neighbourNumber};
   }

   /**
    * marks vertex as removed by setting row and column with that number to -1 in adjacency matrix
    * @param adjacencyMatrix
    * @param smallestLeaf
    */
   private void markVertexRemoved(int[][] adjacencyMatrix, Integer smallestLeaf) {
      //viskame tipu puust valja
      int length = adjacencyMatrix.length;
      //itereerime terve maatriksi pohiselt
      for(int i = 0; i < length; ++i) {
         for(int j = 0; j < length; ++j) {
            //siis kui selle rea voi veeru indeks klapib siis kirjutame sinna -1
            //kuna seal niikaua võiksid olla kas 0 voi 1
            //sellega tuleb, et niimodi meie märgistame kustutava tipu maaratud rea ja 1 veeru numbri
            if(i == smallestLeaf || j == smallestLeaf)
               adjacencyMatrix[i][j] = -1;
         }
      }
   }

   // didn't use this class
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

   }


   // didn't use this class
   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                       + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                       + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

   }
}